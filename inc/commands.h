/*
 * commands.h
 *
 *  Created on: Sep 24, 2012
 *      Author: renan
 */

#ifndef COMMANDS_H_
#define COMMANDS_H_

#include "types.h"
#include "uart0.h"
#include "i2c.h"
#include "XL345.h"
#include "adxl345.h"
#include "sysTick.h"
#include <string.h>
#include <stdio.h>

struct commands
{
	const char* name;
	void (*funct)(void *);
};

struct registers
{
	const char *regName;
	uint8_t reg;
};

Bool parseCommand (const char* str, void *param);

void help ();
void initI2C ();
void initAccel ();
void readxyz ();
void test (void *p);
void readAllRegisters ();
void writeAll ();
void readReg (void *r);

#endif /* COMMANDS_H_ */
