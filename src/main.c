/*
===============================================================================
 Name        : main.c
 Author      : 
 Version     :
 Copyright   : Copyright (C) 
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>

#include "types.h"
//#include "i2c.h"
//#include "XL345.h"
//#include "adxl345.h"
//#include "sysTick.h"
#include "commands.h"

// Variable to store CRP value in. Will be placed automatically
// by the linker when "Enable Code Read Protect" selected.
// See crp.h header for more information
__CRP const unsigned int CRP_WORD = CRP_NO_CRP ;

int main ()
{
	SystemCoreClockUpdate();

	UART0_Init (115200);
	printf ("UART0 initialized\n");
	initSysTick ();

	char received [50];
	char param [50];
	memset (received, 0x00, sizeof (received));
	memset (param, 0x00, sizeof (param));
	char c;
	int loop = 0;
	int loop2 = 0;
	while (1)
	{
		c = UART0_Getchar();	// wait for next character from terminal
		if (c != EOF)
		{
			if ('(' == c)
			{
				c = UART0_Getchar();	// wait for next character from terminal
				while (')' != c)
				{
					param [loop2] = c;
					loop2++;
					c = UART0_Getchar();	// wait for next character from terminal
				}
			}
			else if ('\r' == c)
			{
				if (parseCommand (received, (void *)param))
				{
					loop = 0;
					loop2 = 0;
					memset (received, 0x00, sizeof (received));
					memset (param, 0x00, sizeof (param));
				}
			}
			else
			{
				received [loop] = c;
				loop++;
			}
		}
	}
	return 0;
}


//	if ( I2CInit() == FALSE )	/* initialize I2c */
//	{
//		UART0_PrintString ("Could not initialize I2C0....\r\n");
//		while ( 1 );				/* Fatal error */
//	}
//
//	UART0_PrintString ("Trying to initialize adxl 345...\r\n");
//
//	adxl345_init_alt ();
////	adxl345_init ();
//
//	char msg[50];
//
//	while (1)
//	{
//		adxl345_xyz (&x, &y, &z);
//		memset (msg, 0x00, sizeof (msg));
//		sprintf (msg, "x = 0x%X(%d), y = 0x%X(%d), z = 0x%X(%d)\r", x, x, y, y, z, z);
//		UART0_PrintString (msg);
//
//		ms_delay (500);
//	}
//
//	return 0;
