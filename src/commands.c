/*
 * commands.c
 *
 *  Created on: Sep 24, 2012
 *      Author: renan
 */

#include "commands.h"


static struct commands cmds[] =
{
	{".help", help},
	{"initI2C", initI2C},
	{"initAccel", initAccel},
	{"readxyz", readxyz},
	{"test", test},
	{"readAllRegisters", readAllRegisters},
	{"writeAll", writeAll},
	{"readReg", readReg}
};
static struct registers regs[] =
{
	{"XL345_DEVID"     		, 0x00},
	{"XL345_THRESH_TAP"     , 0x1d},
	{"XL345_OFSX"           , 0x1e},
	{"XL345_OFSY"           , 0x1f},
	{"XL345_OFSZ"           , 0x20},
	{"XL345_DUR"            , 0x21},
	{"XL345_LATENT"         , 0x22},
	{"XL345_WINDOW"         , 0x23},
	{"XL345_THRESH_ACT"     , 0x24},
	{"XL345_THRESH_INACT"   , 0x25},
	{"XL345_TIME_INACT"     , 0x26},
	{"XL345_ACT_INACT_CTL " , 0x27},
	{"XL345_THRESH_FF"      , 0x28},
	{"XL345_TIME_FF"        , 0x29},
	{"XL345_TAP_AXES"       , 0x2a},
	{"XL345_ACT_TAP_STATUS" , 0x2b},
	{"XL345_BW_RATE"        , 0x2c},
	{"XL345_POWER_CTL"      , 0x2d},
	{"XL345_INT_ENABLE"     , 0x2e},
	{"XL345_INT_MAP"        , 0x2f},
	{"XL345_INT_SOURCE"     , 0x30},
	{"XL345_DATA_FORMAT"    , 0x31},
	{"XL345_DATAX0"         , 0x32},
	{"XL345_DATAX1"         , 0x33},
	{"XL345_DATAY0"         , 0x34},
	{"XL345_DATAY1"         , 0x35},
	{"XL345_DATAZ0"         , 0x36},
	{"XL345_DATAZ1"         , 0x37},
	{"XL345_FIFO_CTL"       , 0x38},
	{"XL345_FIFO_STATUS"    , 0x39}
};


void help ()
{
	char * str = "initI2C\nreadAllRegisters\nwriteAll\ninitAccel\nreadxyz\n";
	printf (str);
}

void test (void *p)
{
	char *x = (char *)p;
	int z = (int)*x;
	printf ("parameter = %d", z);
}

void initI2C ()
{
	if (I2CInit() == FALSE)	// initialize I2c
	{
		printf ("Could not initialize I2C0....\n");
		while (1);				// Fatal error
	}
	printf ("I2C initialized\n");
}
void initAccel ()
{
	adxl345_init_alt ();
	printf ("Accelerometer initialized\n");
}

void readxyz ()
{
	char msg[50];
	short x;
	short y;
	short z;
	int loop;
	for (loop = 0; loop < 10; loop++)
	{
		adxl345_xyz (&x, &y, &z);
		memset (msg, 0x00, sizeof (msg));
		sprintf (msg, "x = 0x%X(%d), y = 0x%X(%d), z = 0x%X(%d)\r", x, x, y, y, z, z);
		printf (msg);
		msDelay (500);
	}
}
void writeAll()
{
	int ret = FALSE;
	int loop = 1;
	while (loop < sizeof (regs) / sizeof (regs[0]))
	{
		ret = i2c0_write_one_byte (XL345_ALT_ADDR, regs[loop].reg, loop);
		if (!ret)
			printf ("%s [%x] = Could not write register.\n", regs[loop].regName, regs[loop].reg);
		loop++;
	}
}

void readAllRegisters ()
{
	unsigned char data;
	int ret = FALSE;
	int loop = 0;
	while (loop < sizeof (regs) / sizeof (regs[0]))
	{
		ret = i2c0_read_one_byte (XL345_ALT_ADDR, regs[loop].reg, &data);
		if (ret)
			printf ("%s [%x] = 0x%X\n", regs[loop].regName, regs[loop].reg, data);
		else
			printf ("%s [%x] = Could not read register.\n", regs[loop].regName, regs[loop].reg);
		loop++;
	}
}

void readReg (void *r)
{
	unsigned char data[1];
	memset (data, 0x00, sizeof (data));
	int ret = FALSE;
	unsigned char reg = (*(char*)r) - '0';
	ret = i2c0_read_one_byte (XL345_ALT_ADDR, reg, data);
	if (ret)
		printf ("0x%X", *data);
}


Bool parseCommand (const char *str, void * param)
{
	Bool ret = FALSE;
	int i;

	for (i = 0; i != sizeof (cmds) / sizeof (cmds[0]); ++i)
	{
		if(strcmp (str, cmds[i].name) == 0)
		{
			(*cmds[i].funct)(param);
			ret = TRUE;
			break;
		}
	}

	return ret;
}
